<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.geometry.*?>
<?import javafx.scene.control.*?>
<?import java.lang.*?>
<?import javafx.scene.layout.*?>

<?import java.net.URL?>
<GridPane styleClass="form" maxHeight="-Infinity" maxWidth="-Infinity" minHeight="-Infinity" minWidth="-Infinity" prefHeight="${util.rows(table.columnList)?size * 30 + 30}" prefWidth="645.0"
          fx:controller="${controllerPackage}.${util.firstLower(table.javaName)}.${table.javaName}SearchController" xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1">
    <children>
        <#list util.rows(table.columnList) as row>
            <#list 0..3 as t>
                <#if util.column(table.columnList, (row_index * 4) + t)??>
                    <#if util.isDate(util.column(table.columnList, (row_index * 4) + t))>
        <Label text="${util.comment(util.column(table.columnList, (row_index * 4) + t))}" styleClass="form_label" GridPane.columnIndex="${t_index * 3}" GridPane.rowIndex="${row_index}" />
        <DatePicker fx:id="${util.column(table.columnList, (row_index * 4) + t).property}DatePicker" GridPane.columnIndex="${t_index * 3 +1}" GridPane.rowIndex="${row_index}" GridPane.columnSpan="2"/>
                    <#elseif util.column(table.columnList, (row_index * 4) + t).foreign>
        <Label text="${util.comment(util.column(table.columnList, (row_index * 4) + t))}" styleClass="form_label" GridPane.columnIndex="${t_index * 3}" GridPane.rowIndex="${row_index}" />
        <TextField fx:id="${util.column(table.columnList, (row_index * 4) + t).property}Text" styleClass="form_text" GridPane.columnIndex="${t_index * 3+1}" GridPane.rowIndex="${row_index}" />
        <Button text="select" GridPane.columnIndex="${t_index * 3+2}" GridPane.rowIndex="${row_index}"/>
                    <#elseif util.isEnum(util.column(table.columnList, (row_index * 4) + t))>
        <Label text="${util.comment(util.column(table.columnList, (row_index * 4) + t))}" styleClass="form_label" GridPane.rowIndex="${row_index}" GridPane.columnIndex="${t_index * 3}"/>
        <ChoiceBox fx:id="${util.column(table.columnList, (row_index * 4) + t).property}ChoiceBox" styleClass="form_text" GridPane.columnIndex="${t_index * 3 + 1}" GridPane.rowIndex="${row_index}" GridPane.columnSpan="2"/>
                    <#else>
        <Label text="${util.comment(util.column(table.columnList, (row_index * 4) + t))}" styleClass="form_label" GridPane.columnIndex="${t_index * 3}" GridPane.rowIndex="${row_index}" />
        <TextField fx:id="${util.column(table.columnList, (row_index * 4) + t).property}Text" styleClass="form_text" GridPane.columnIndex="${t_index * 3+1}" GridPane.rowIndex="${row_index}" GridPane.columnSpan="2"/>
                    </#if>
                </#if>
            </#list>

        </#list>

        <Button text="Search" onAction="#searchAct" GridPane.rowIndex="${util.rows(table.columnList)?size}"/>
    </children>
    <rowConstraints>
        <RowConstraints maxHeight="30.0" minHeight="30.0" prefHeight="30.0" />
    </rowConstraints>
    <columnConstraints>
        <#list 0..3 as t>
        <ColumnConstraints prefWidth="100" halignment="RIGHT"/>
        <ColumnConstraints prefWidth="150"/>
        <ColumnConstraints prefWidth="40"/>
        </#list>
    </columnConstraints>
    <padding>
        <Insets bottom="6.0" left="16.0" right="20.0" top="6.0" />
    </padding>
    <stylesheets>
        <URL value="@/css/common.css"/>
    </stylesheets>
</GridPane>
