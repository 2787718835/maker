package ${controllerPackage}.${util.firstLower(table.javaName)};

import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import ldh.fx.component.table.function.ValuedEnumStringConverter;

import ${pojoPackage}.${table.javaName};
import ${pojoPackage}.where.${table.javaName}Where;
<#list table.columnList as column>
    <#if util.isEnum(column)>
import ${projectPackage}.constant.${column.javaType};
    </#if>
</#list>
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.ResourceBundle;

/**
* Created by ldh on 2017/4/17.
*/
@FXMLController
public class ${table.javaName}SearchController implements Initializable{

    @Resource
    private ${table.javaName}MainController ${util.firstLower(table.javaName)}MainController;

    <#list table.columnList as column>
        <#if util.isDate(column)>
    @FXML private DatePicker ${column.property}DatePicker;
        <#elseif util.isEnum(column)>
    @FXML private ChoiceBox ${column.property}ChoiceBox;
        <#else>
    @FXML private TextField ${column.property}Text;
        </#if>
    </#list>

    @FXML private void searchAct() {
        ${util.firstLower(table.javaName)}MainController.setSearchParam(buildParams());
        ${util.firstLower(table.javaName)}MainController.load(null);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        <#list table.columnList as column>
            <#if util.isEnum(column)>
        ${column.property}ChoiceBox.getItems().addAll(${column.javaType}.values());
        ${column.property}ChoiceBox.setConverter(new ValuedEnumStringConverter());
            </#if>
        </#list>
    }

    public ${table.javaName}Where buildParams(){
        ${table.javaName}Where ${util.firstLower(table.javaName)}Where = new ${table.javaName}Where();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = null;
        <#list table.columnList as column>
            <#if util.isDate(column)>
        if (${column.property}DatePicker.getValue() != null) {
            LocalDate ${column.property}Date = ${column.property}DatePicker.getValue();
            zdt = ${column.property}Date.atStartOfDay(zoneId);
            ${util.firstLower(table.javaName)}Where.set${util.firstUpper(column.property)}(Date.from(zdt.toInstant()));
        }
            <#elseif util.isPrimaryKey(table, column)>

            <#elseif util.isEnum(column)>
            ${column.javaType} ${column.property} = (${column.javaType}) ${column.property}ChoiceBox.getSelectionModel().getSelectedItem();
        if (${column.property} != null) {
            ${util.firstLower(table.javaName)}Where.set${util.firstUpper(column.property)}(${column.property});
        }
            <#elseif column.foreign>
            //            <#--paramMap.put("${column.property}.${column.foreignKey.foreignTable.primaryKey.columns[0].property}", ${column.property}Text.getText());-->
            <#elseif util.isBigDecimal(column)>
        if (!StringUtils.isEmpty((${column.property}Text.getText().trim()))) {
            ${util.firstLower(table.javaName)}Where.set${util.firstUpper(column.property)}(new BigDecimal(${column.property}Text.getText().trim()));
        }
            <#else>
        if (!StringUtils.isEmpty((${column.property}Text.getText().trim()))) {
            ${util.firstLower(table.javaName)}Where.set${util.firstUpper(column.property)}(${column.javaType}.valueOf(${column.property}Text.getText()));
        }
            </#if>
        </#list>

        return ${util.firstLower(table.javaName)}Where;
    }
}
