package ldh.maker.code;

import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.db.EnumDb;
import ldh.maker.freemaker.*;
import ldh.maker.util.*;
import ldh.maker.vo.JavafxSetting;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;


/**
 * Created by ldh on 2017/4/6.
 */
public abstract class JavafxCreateCode extends CreateCode{

    protected String javafxPath = "";

    public JavafxCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    protected void copyResource() {
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        try {
            copyResources("css", dirs);
            copyResources("images", dirs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void saveJson(String fileName, String json, String... packs) throws IOException {
        String jsonPath = createResourcePath(packs);
        String file = jsonPath + "/" + fileName;
        FileUtil.saveFile(file, json);
    }

    protected void buildUtilFile(TableInfo tableInfo, String packageDir, String ftl, String javaFileName) {
        String fxml = createResourcePath("fxml");
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + "." + packageDir);
        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl(ftl)
                .fileName(javaFileName)
                .outPath(path)
                .make();
    }

    protected JavafxPojoMaker buildJavafxPojoMaker(Table table, String pojoPath, Class<?> key, KeyMaker keyMaker) {
        return (JavafxPojoMaker) new JavafxPojoMaker()
                .pack(javafxPath)
                .table(table)
                .key(key, keyMaker)
                .outPath(pojoPath);
    }

    protected void buildJavafxEditFormMaker(Table table, String ftl) {
        String path = createResourcePath("fxml", "module", FreeMakerUtil.firstLower(table.getJavaName()));
        String projectPackage = this.getProjectRootPackage(javafxPath);
        new JavafxEditFormMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();
    }

    protected void buildJavafxMainFormMaker(Table table, String ftl) {
        String fxml = createResourcePath("fxml", "module", FreeMakerUtil.firstLower(table.getJavaName()));
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String fileName = table.getJavaName() + "Main.fxml";
        new JavafxMaker()
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .table(table)
                .ftl(ftl)
                .fileName(fileName)
                .outPath(fxml)
                .make();
    }

    protected void buildJavafxMainControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        new JavafxMainControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .servicePackage(data.getServicePackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(data.getControllerPackageProperty() + "." + FreeMakerUtil.firstLower(table.getJavaName()))
                .table(table)
                .className(table.getJavaName() + "MainView")
                .fileName(table.getJavaName() + "MainView.java")
                .outPath(path)
                .fxml("/fxml/module/" + FreeMakerUtil.firstLower(table.getJavaName()) + "/" + table.getJavaName() + "Main.fxml")
                .make();
    }

    protected void buildJavafxEditControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        new JavafxEditControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .ftl(ftl)
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(data.getControllerPackageProperty() + "." + FreeMakerUtil.firstLower(table.getJavaName()))
                .table(table)
                .className(table.getJavaName() + "EditFormView")
                .fileName(table.getJavaName() + "EditFormView.java")
                .outPath(path)
                .fxml("/fxml/module/" + FreeMakerUtil.firstLower(table.getJavaName()) + "/" + table.getJavaName() + "EditForm.fxml")
                .make();
    }

    protected void buildJavafxSearchFormMaker(Table table, String ftl) {
        String path = createResourcePath("fxml", "module", FreeMakerUtil.firstLower(table.getJavaName()));
        String projectPackage = this.getProjectRootPackage(javafxPath);
        new JavafxSearchFormMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();
    }

    protected void buildJavafxSearchControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        new JavafxSearchControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .ftl(ftl)
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(data.getControllerPackageProperty() + "." + FreeMakerUtil.firstLower(table.getJavaName()))
                .table(table)
                .className(table.getJavaName() + "SearchFormView")
                .fileName(table.getJavaName() + "SearchFormView.java")
                .outPath(path)
                .fxml("/fxml/module/" + FreeMakerUtil.firstLower(table.getJavaName()) + "/" + table.getJavaName() + "SearchForm.fxml")
                .make();
    }

    protected void buildPomXmlMaker() {
        String projectRootPackage = getProjectRootPackage(javafxPath);
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .project(this.getProjectName())
                .outPath(resourcePath)
                .ftl("client/pom.ftl")
                .make();
    }

    protected String createResourcePath(String... packs) {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "main", "resources"));
        for(String pack : packs) {
            dirs.add(pack);
        }
        path = makePath(path, dirs);
        return path;
    }

    protected void copyResources(String srcDir, List<String> dirs) throws IOException {
        String root = FileUtil.getSourceRoot();
        Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(srcDir);
        String srcFile = urls.nextElement().getFile();
        CopyDirUtil.copyResourceDir(srcFile, root, dirs);
    }

    protected void copyFile(String file, List<String> dirs) throws IOException {
        String root = FileUtil.getSourceRoot();
        URL url = Thread.currentThread().getContextClassLoader().getResource(file);
        String srcFile = url.getFile();
        CopyDirUtil.copyFile(srcFile, root, dirs);
    }
}
