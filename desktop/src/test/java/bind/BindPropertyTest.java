package bind;

import com.google.gson.reflect.TypeToken;
import javafx.application.Platform;
import javafx.beans.property.*;
import ldh.common.PageResult;
import ldh.common.Pageable;
import ldh.common.json.JsonViewFactory;
import ldh.fx.util.DialogUtil;
import ldh.fx.util.JsonHttpUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Created by ldh123 on 2018/6/1.
 */
public class BindPropertyTest {

    @Test
    public void nullTest() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        IntegerProperty id = new SimpleIntegerProperty();
        System.out.println(id.get());

        StringProperty name = new SimpleStringProperty();
        System.out.println(name.get());

        ObjectProperty<Student> student = new SimpleObjectProperty<>();
        System.out.println(student.get());

//        BeanUtils.describe();
        task(()-> {System.out.println("sfadfas"); return null;});

        String url = "";
        Map paramMap = BeanUtils.describe(student);
        PageResult<Student> pageResult = JsonHttpUtil.get(JsonViewFactory.create(), url, paramMap, new TypeToken<PageResult<Student>>() {}.getType());
    }

    private void task(Supplier<?> supplier) {
        supplier.get();
    }

    public static class Student {
        IntegerProperty id = new SimpleIntegerProperty();

        StringProperty name = new SimpleStringProperty();

        public int getId() {
            return id.get();
        }

        public IntegerProperty idProperty() {
            return id;
        }

        public void setId(int id) {
            this.id.set(id);
        }

        public String getName() {
            return name.get();
        }

        public StringProperty nameProperty() {
            return name;
        }

        public void setName(String name) {
            this.name.set(name);
        }
    }

    private PageResult<Student> loadPageResult(Pageable pageable) {
        try {
            Map paramMap = BeanUtils.describe(pageable);
            String url = "/achievement/list/json";
            PageResult<Student> pageResult = JsonHttpUtil.get(JsonViewFactory.create(), url, paramMap, new TypeToken<PageResult<Student>>() {}.getType());
//            Platform.runLater(()->achievementGridTable.setData(pageResult));
            return pageResult;
        } catch (Exception var2) {
            DialogUtil.info("查询数据失败", var2.getMessage(), 300, 220);
            return null;
        }

    }
}
