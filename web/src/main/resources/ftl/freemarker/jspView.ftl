<${r'#'}include "/macro/publicMacro.ftl">
<${r'#'}import "/macro/pagination.ftl" as Pagination>

<${r'@'}header title="${util.comment(table)}简介">
    <link href="/resource/common/css/pagination.css" rel="stylesheet">
    <script src="/resource/common/js/pagination.js"></script>
</${r'@'}header>

<${r'@'}body>
<h2>${util.comment(table)}详情</h2>
<div class="table-responsive">
    <table class="table">
        <thead>
            <th width="100">名称</th>
            <th>值</th>
        </thead>
        <tbody>
        <#list table.columnList as column>
            <tr>
                <th>${util.comment(column)}</th>
                <#if column.foreign>
                <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'!}'}</th>
                <#elseif util.isDate(column)>
                <th>${r'${('}${util.firstLower(table.javaName)}.${column.property}?string('yyyy-MM-dd hh:mm:ss')${r')!}'} </th>
                <#elseif util.isNumber(column)>
                <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'?number}'}</th>
                <#elseif util.isEnum(column)>
                <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</th>
                <#else>
                <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</th>
                </#if>
            </tr>
        </#list>
        </tbody>
    </table>
</div>
</${r'@'}body>

<${r'@'}footer>

</${r'@'}footer>