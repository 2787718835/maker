package main;

import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.MakerInfo;

public class MysqlMaker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String outPath = "E:\\project\\ldh\\maker-mysql-test\\src\\main\\java";
		
//		MyMapper other = new MyMapper();
//		other.table("scenic").other("status", Status.class);
//		other.all("status", Status.class);
		TableInfo ti = new TableInfo(null, null, null);
		
		MakerInfo mi = new MakerInfo();
		mi.mybatisConfigPath("E:\\project\\eclipse\\recipe\\resource\\mybatis-config.xml")
		  .daoPackage("com.role.dao")
		  .pojoPackage("com.role.pojo")
		  .pojoWherePackage("com.role.controller.pojo")
		  .servicePackage("com.role.service")
		  .serviceImplPackage("com.role.service.impl")
		  .controllerPackage("com.role.admin.controller")
		  .srcPath(outPath)
		  .xmlPath("E:\\project\\ldh\\maker-mysql-test\\src\\main\\resources")
		  .jspPath("E:\\project\\ldh\\maker-mysql-test\\src\\main\\webapp\\WEB-INF\\view\\")
		  .jsPath("E:\\project\\ldh\\maker-mysql-test\\src\\main\\webapp\\resource\\js")
		  .jspFtl("jspList.ftl")
		  .jsFtl("jsList.ftl")
		  .tableInfo(ti);
		mi.make();
		
//		MyMapper other = new MyMapper();
//		other.table("scenic").other("status", Status.class);
//		TableInfo mi = new TableInfo(other);
//		
//		Table table = mi.getTable("gather");
//		
//		PojoMaker bm = new PojoMaker()
//		 	.pack("com.recipe.pojo")
//		 	.extend(CommonEntity.class, "CommonEntity<Integer>")
//		 	.table(table)
//		 	.outPath(outPath)
//		 	.make();
//		
//		
//		new PojoWhereMaker()
//		 	.pack("com.recipe.pojo.where")
//		 	.outPath(outPath)
//		 	.className(FreeMakerUtil.beanName(table.getName()) + "Where")
//		 	.extend(bm)
//		 	.implement(Pageable.class)
//		 	.serializable(bm.isSerializable())
//		 	.make();
//		
//		new DaoMaker()
//		 	.pack("com.recipe.dao")
//		 	.outPath(outPath)
//		 	.className(FreeMakerUtil.beanName(table.getName() + "Dao"))
//		 	.beanWhereMaker(bm)
//		 	.make();
//
	}

}
