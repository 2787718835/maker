package ldh.maker.freemaker;

/**
 * Created by ldh on 2017/4/8.
 */
public class SimpleJavaMaker extends FreeMarkerMaker<SimpleJavaMaker> {

    protected String projectRootPackage;

    public SimpleJavaMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public SimpleJavaMaker data(String key, Object value) {
        data.put(key, value);
        return this;
    }

    @Override
    public SimpleJavaMaker make() {
        data();
        this.out(ftl, data);
        return this;
    }

    @Override
    public void data() {
        data.put("projectRootPackage", projectRootPackage);
    }
}
