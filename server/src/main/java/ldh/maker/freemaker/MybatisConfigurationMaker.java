package ldh.maker.freemaker;

import ldh.maker.vo.DBConnectionData;

/**
 * Created by ldh on 2017/4/8.
 */
public class MybatisConfigurationMaker extends FreeMarkerMaker<MybatisConfigurationMaker> {

    protected String projectRootPackage;
    protected String mybatisXmlPackage;
    protected String daoPackage;

    public MybatisConfigurationMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public MybatisConfigurationMaker mybatisXmlPackage(String mybatisXmlPackage) {
        this.mybatisXmlPackage = mybatisXmlPackage;
        return this;
    }

    public MybatisConfigurationMaker daoPackage(String daoPackage) {
        this.daoPackage = daoPackage;
        return this;
    }

    @Override
    public MybatisConfigurationMaker make() {
        data();
        this.out("mybatisConfiguration.ftl", data);
        return this;
    }

    @Override
    public void data() {
        fileName = "MybatisConfiguration.java";
        data.put("projectRootPackage", projectRootPackage);
        data.put("mybatisXmlPackage", mybatisXmlPackage);
        data.put("daoPackage", daoPackage);
    }
}
