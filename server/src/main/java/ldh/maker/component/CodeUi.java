package ldh.maker.component;

import javafx.application.Platform;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.db.SettingDb;
import ldh.maker.util.*;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.sql.SQLException;

public class CodeUi extends TabPane {

    protected TreeItem<TreeNode> treeItem;
    protected String dbName;
    protected String tableName;

//    protected SettingPane settingPane;
    protected ColumnUi columnPane;

    protected CodePane servicePane;
    protected CodePane daoPane;
    protected CodePane controllerPane;
    protected CodePane pojoPane;
    protected XmlPane mybatisXmlPane;


    public CodeUi(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        this.treeItem = treeItem;
        this.dbName = dbName;
        this.tableName = tableName;

        DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName).loanTable(tableName);

//        settingPane = new SettingPane(treeItem, dbName);
        columnPane = createColumnPane(treeItem, dbName, tableName);
        init();
    }

    protected ColumnUi createColumnPane(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        return new ColumnUi(treeItem, dbName, tableName, this);
    }

    private void init() {
//        Tab settingTab = new Tab("设置");
//        settingTab.setContent(settingPane);
//        settingTab.setClosable(false);

        Tab columnTab = new Tab("数据");
        columnTab.setContent(columnPane);
        columnTab.setClosable(false);

        this.getTabs().addAll(columnTab);
        this.setSide(Side.BOTTOM);
//        if (settingPane.isSetting()) {
//            this.getSelectionModel().select(columnTab);
//        }
    }

    public void createCode(Table table, boolean isReload) throws SQLException {
        createPojo(table, isReload);
    }

    private void createPojo(Table table, boolean isReload) throws SQLException {
        CreateCode createCode = buildCreateCode(table);
        createCode.create();
        if (this.getParent() == null) return;
        if (!isReload) return;
        Platform.runLater(()->{
            try {
                loadCode(table, createCode);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void loadCode(Table table, CreateCode createCode) throws SQLException {
        SettingData settingData = SettingDb.loadData(treeItem.getParent().getValue(), dbName);
        if (settingData == null) return;

        String pojoName = table.getJavaName() + ".java";
        String daoName = table.getJavaName() + "Dao.java";
        String serviceName = table.getJavaName() + "Service.java";
        String controllerName = table.getJavaName() + "Controller.java";
        pojoPane = new CodePane(treeItem, pojoName, settingData.getPojoPackageProperty(), createCode);
        daoPane = new CodePane(treeItem, daoName, settingData.getDaoPackageProperty(), createCode);
        servicePane = new CodePane(treeItem, serviceName, settingData.getServicePackageProperty(), createCode);
        controllerPane = new CodePane(treeItem, controllerName, settingData.getControllerPackageProperty(), createCode);
        mybatisXmlPane = new XmlPane(treeItem, table.getJavaName() + "Dao.xml", settingData.getXmlPackageProperty(), createCode);

        Tab pojoTab = new Tab("POJO");
        pojoTab.setContent(pojoPane);
        pojoTab.setClosable(false);

        Tab xmlTab = new Tab("MybatisXml");
        xmlTab.setContent(mybatisXmlPane);
        xmlTab.setClosable(false);

        Tab daoTab = new Tab("Dao");
        daoTab.setContent(daoPane);
        daoTab.setClosable(false);

        Tab serviceTab = new Tab("Service");
        serviceTab.setContent(servicePane);
        serviceTab.setClosable(false);

        Tab controllerTab = new Tab("Controller");
        controllerTab.setContent(controllerPane);
        controllerTab.setClosable(false);

        if (table.getName().equals(tableName)) {
            this.getTabs().addAll(pojoTab, xmlTab, daoTab, serviceTab, controllerTab);
        } else {
            ContentUi contentUi = UiUtil.getContentUi();
            if (!(contentUi instanceof TableUi)) return;
            TableUi tableUi = (TableUi) contentUi;
            Tab tab = tableUi.createContentTab(table.getName());
            CodeUi codeUi = (CodeUi) tab.getContent();
            this.getTabs().addAll(pojoTab, xmlTab, daoTab, serviceTab, controllerTab);
            tableUi.getTabPane().getTabs().add(tab);
            tableUi.getTabPane().getSelectionModel().select(tab);
            codeUi.getTabs().addAll(pojoTab, xmlTab, daoTab, serviceTab, controllerTab);
        }
    }

    protected CreateCode buildCreateCode(Table table) throws SQLException {
        SettingData data = SettingDb.loadData(treeItem.getValue().getParent(), dbName);
        CreateCode createCode = new CreateCode(data, treeItem, dbName, table);
        return createCode;
    }
}
