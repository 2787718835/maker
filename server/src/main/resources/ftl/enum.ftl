package ${package};

import ldh.common.mybatis.ValuedEnum;

public enum ${className} <#if type??>implements ValuedEnum<${type}></#if> {
    <#list enumValues?keys as key>
    <#if type??>
    <#if isString || type == 'Integer'>
    ${key}(${enumValues[key].value}, "${enumValues[key].desc}"),
    <#elseif type == 'Boolean'>
    ${key}((${util.firstLower(type)})${enumValues[key].value?string("true","false")}, "${enumValues[key].desc}"),
    <#else>
    ${key}((${util.firstLower(type)})${enumValues[key].value}, "${enumValues[key].desc}"),
    </#if>
    <#else>
    ${key}("${enumValues[key].desc}"),
    </#if>
    </#list>
    ;

    <#if type??>
    private ${type} value;
    private String desc;

    private ${className}(${type} value, String desc){
        this.value = value;
        this.desc = desc;
    }

    @Override
    public ${type} getValue() {
        return value;
    }
    <#else>
    private StudentStatusEnum(String desc){
        this.desc = desc;
    }
    </#if>

    public String getDesc() {
        return desc;
    }
}
