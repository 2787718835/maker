# datasource
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.url = jdbc:mysql://${dbConnectionData.ipProperty}:${dbConnectionData.portProperty?c}/${dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&zeroDateTimeBehavior=convertToNull
spring.datasource.username = ${dbConnectionData.userNameProperty}
spring.datasource.password = ${dbConnectionData.passwordProperty}
spring.datasource.driverClassName = com.mysql.cj.jdbc.Driver
spring.datasource.maxActive = 25
spring.datasource.minIdle = 3
spring.datasource.initialSize = 10
spring.datasource.connectionTimeout = 30000
spring.datasource.maxWait = 30000
spring.datasource.minEvictableIdleTimeMillis = 30000

# 页面默认前缀目录
spring.mvc.view.prefix=/WEB-INF/jsp/
# 响应页面默认后缀
spring.mvc.view.suffix=.jsp


spring.mvc.static-path-pattern=/resource/**
spring.resources.static-locations=/resource/
server.port=8080